﻿using UnityEngine;
using System.Collections;

public class PlanetGravityController : MonoBehaviour {
	[SerializeField]
	private static float range = 1000.0f;

	// Use this for initialization
	void Start () {
	}
	
	void FixedUpdate() {
		if (Input.GetMouseButton(0)) {
			Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Collider2D[] colliders = Physics2D.OverlapCircleAll(mousePosition, range);
			foreach (Collider2D coll in colliders) {
				if (gameObject == coll.gameObject) {
					gameObject.rigidbody2D.AddForce(new Vector2(50 * (mousePosition.x - gameObject.transform.position.x), 100 * (mousePosition.y - gameObject.transform.position.y)));
				}
			}
		}
	}
}
