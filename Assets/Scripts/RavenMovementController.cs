﻿using UnityEngine;
using System.Collections;

public class RavenMovementController : MonoBehaviour {
	[SerializeField]
	private float speed = 2.0f;

	// Use this for initialization
	void Start() {
	
	}
	
	// Update is called once per frame
	void Update() {
		float translation = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
		transform.Translate(translation, 0, 0);
	}
}
