﻿using UnityEngine;
using System.Collections;

public class WorldGravityController : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Physics2D.gravity = new Vector3(0, 1.0f, 0);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
